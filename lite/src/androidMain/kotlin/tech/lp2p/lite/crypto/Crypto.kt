package tech.lp2p.lite.crypto

import org.kotlincrypto.random.CryptoRand


const val SECRET_KEY_LEN: Int = Field25519.FIELD_LEN
const val PUBLIC_KEY_LEN: Int = Field25519.FIELD_LEN
const val SIGNATURE_LEN: Int = Field25519.FIELD_LEN * 2


/**
 * Returns a random byte array of size `size`.
 */

fun randBytes(size: Int): ByteArray {
    return CryptoRand.Default.nextBytes(ByteArray(size))
}


/**
 * @param data the byte array to be wrapped.
 * @return an immutable wrapper around the provided bytes.
 */
fun copyFrom(data: ByteArray): Bytes {
    return copyFrom(data, 0, data.size)
}

/**
 * Wrap an immutable byte array over a slice of a Bytes
 *
 * @param data  the byte array to be wrapped.
 * @param start the starting index of the slice
 * @param length   the length of the slice. If start + len is larger than the size of `data`, the
 * remaining data will be returned.
 * @return an immutable wrapper around the bytes in the slice from `start` to `start +
 * len`
 */
fun copyFrom(data: ByteArray, start: Int, length: Int): Bytes {
    var len = length
    if (start + len > data.size) {
        len = data.size - start
    }
    return Bytes(data, start, len)
}

/**
 * Best effort fix-timing array comparison.
 *
 * @return true if two arrays are equal.
 */

fun equal(x: ByteArray, y: ByteArray): Boolean {
    return x.contentEquals(y)
}

/**
 * Returns the concatenation of the input arrays in a single array. For example, `concat(new
 * byte[] {a, b}, new byte[] {}, new byte[] {c}` returns the array `{a, b, c}`.
 *
 * @return a single array containing all the values from the source arrays, in order
 */
fun concat(vararg chunks: ByteArray): ByteArray {
    var length = 0
    for (chunk in chunks) {
        if (length > Int.MAX_VALUE - chunk.size) {
            throw Exception("exceeded size limit")
        }
        length += chunk.size
    }
    val res = ByteArray(length)
    var pos = 0
    for (chunk in chunks) {
        chunk.copyInto(res, pos, 0, chunk.size)
        pos += chunk.size
    }
    return res
}