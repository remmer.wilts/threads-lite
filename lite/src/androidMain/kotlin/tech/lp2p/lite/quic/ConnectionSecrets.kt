package tech.lp2p.lite.quic

import kotlinx.atomicfu.atomic
import org.kotlincrypto.macs.hmac.sha2.HmacSHA256
import tech.lp2p.lite.tls.CipherSuite
import tech.lp2p.lite.tls.TrafficSecrets

open class ConnectionSecrets(protected val version: Int) {
    private val clientSecretsInitial = atomic<Keys?>(null)
    private val serverSecretsInitial = atomic<Keys?>(null)

    private val clientSecretsHandshake = atomic<Keys?>(null)
    private val serverSecretsHandshake = atomic<Keys?>(null)

    private val clientSecretsApp = atomic<Keys?>(null)
    private val serverSecretsApp = atomic<Keys?>(null)


    /**
     * Generate the initial secrets
     */
    fun computeInitialKeys(dcid: Number) {
        val initialSecret = computeInitialSecret(version, dcid)

        clientSecretsInitial.value = Keys.createInitialKeys(version, initialSecret, true)
        serverSecretsInitial.value = Keys.createInitialKeys(version, initialSecret, false)
    }

    private fun createKeys(
        level: Level,
        secrets: TrafficSecrets,
        selectedCipherSuite: CipherSuite
    ) {
        check(selectedCipherSuite == CipherSuite.TLS_AES_128_GCM_SHA256) { "unsupported cipher suite $selectedCipherSuite" }

        when (level) {
            Level.Handshake -> {
                clientSecretsHandshake.value =
                    Keys.computeHandshakeKeys(version, true, secrets)

                serverSecretsHandshake.value =
                    Keys.computeHandshakeKeys(version, false, secrets)

            }

            Level.App -> {
                clientSecretsApp.value =
                    Keys.computeApplicationKeys(version, true, secrets)

                serverSecretsApp.value =
                    Keys.computeApplicationKeys(version, false, secrets)

            }

            else -> throw RuntimeException("not supported level")
        }
    }

    fun computeHandshakeSecrets(secrets: TrafficSecrets, selectedCipherSuite: CipherSuite) {
        createKeys(Level.Handshake, secrets, selectedCipherSuite)
    }

    fun computeApplicationSecrets(secrets: TrafficSecrets, selectedCipherSuite: CipherSuite) {
        createKeys(Level.App, secrets, selectedCipherSuite)
    }

    fun remoteSecrets(level: Level): Keys? {
        return when (level) {
            Level.Initial -> serverSecretsInitial.value
            Level.App -> serverSecretsApp.value
            Level.Handshake -> serverSecretsHandshake.value
        }
    }

    fun remoteSecrets(level: Level, keys: Keys) {
        when (level) {
            Level.Initial -> serverSecretsInitial.value = keys
            Level.App -> serverSecretsApp.value = keys
            Level.Handshake -> serverSecretsHandshake.value = keys
        }
    }


    fun ownSecrets(level: Level, keys: Keys) {
        when (level) {
            Level.Initial -> clientSecretsInitial.value = keys
            Level.App -> clientSecretsApp.value = keys
            Level.Handshake -> clientSecretsHandshake.value = keys
        }
    }


    fun ownSecrets(level: Level): Keys? {
        return when (level) {
            Level.Initial -> clientSecretsInitial.value
            Level.App -> clientSecretsApp.value
            Level.Handshake -> clientSecretsHandshake.value
        }
    }


    fun discardHandshakeKeys() {
        clientSecretsHandshake.value = null
        serverSecretsHandshake.value = null
    }


    fun discardInitialKeys() {
        clientSecretsInitial.value = null
        serverSecretsInitial.value = null
    }

    fun discardKeys() {
        clientSecretsHandshake.value = null
        serverSecretsHandshake.value = null
        clientSecretsInitial.value = null
        serverSecretsInitial.value = null
        clientSecretsApp.value = null
        serverSecretsApp.value = null
    }


    private fun hmacSHA256(initialSalt: ByteArray, inputKeyingMaterial: ByteArray): ByteArray {
        val mac = HmacSHA256(initialSalt)
        require(inputKeyingMaterial.isNotEmpty()) {
            "provided inputKeyingMaterial must be at least of size 1 and not null"
        }
        return mac.doFinal(inputKeyingMaterial)
    }

    private fun computeInitialSecret(actualVersion: Int, dcid: Number): ByteArray {
        // https://www.rfc-editor.org/rfc/rfc9001.html#name-initial-secrets
        // "The hash function for HKDF when deriving initial secrets and keys is SHA-256"

        val initialSalt = if (Version.isV2(actualVersion))
            Settings.STATIC_SALT_V2 else Settings.STATIC_SALT_V1

        return hmacSHA256(initialSalt, numToBytes(dcid))
    }

}
