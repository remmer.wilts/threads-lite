package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

data class FinishedMessage(val verifyData: ByteArray, override val bytes: ByteArray) :
    HandshakeMessage {
    override val type: HandshakeType
        get() = HandshakeType.FINISHED

    companion object {
        fun createFinishedMessage(hmac: ByteArray): FinishedMessage {
            val buffer = Buffer()
            buffer.writeInt((HandshakeType.FINISHED.value.toInt() shl 24) or hmac.size)
            buffer.write(hmac)
            require(buffer.size.toInt() == 4 + hmac.size)
            return FinishedMessage(hmac, buffer.readByteArray())
        }

        fun parse(buffer: Buffer, data: ByteArray): FinishedMessage {

            val remainingLength = parseHandshakeHeader(
                buffer,
                4 + 32
            )
            val verifyData = buffer.readByteArray(remainingLength)


            return FinishedMessage(verifyData, data)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FinishedMessage

        if (!verifyData.contentEquals(other.verifyData)) return false
        if (!bytes.contentEquals(other.bytes)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = verifyData.contentHashCode()
        result = 31 * result + bytes.contentHashCode()
        return result
    }

}
