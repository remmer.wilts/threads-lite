package tech.lp2p.lite

import kotlinx.atomicfu.atomic
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.withTimeout
import tech.lp2p.asen.Keys
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.PeerStore
import tech.lp2p.asen.Peeraddr
import tech.lp2p.lite.crypto.Ed25519Sign
import tech.lp2p.lite.crypto.Ed25519Verify
import tech.lp2p.lite.protos.RELAY_PROTOCOL_STOP
import tech.lp2p.lite.protos.RelayStopHandler
import tech.lp2p.lite.protos.connect
import tech.lp2p.lite.protos.connectHop
import tech.lp2p.lite.protos.createCertificate
import tech.lp2p.lite.protos.createPeerIdKey
import tech.lp2p.lite.protos.decodePeerIdByName
import tech.lp2p.lite.protos.findClosestPeers
import tech.lp2p.lite.protos.newSignature
import tech.lp2p.lite.protos.prefixToString
import tech.lp2p.lite.protos.relayMessage
import tech.lp2p.lite.protos.reserveHop
import tech.lp2p.lite.quic.Certificate
import tech.lp2p.lite.quic.Connection
import tech.lp2p.lite.quic.Connector
import java.util.concurrent.ConcurrentHashMap


internal const val DHT_ALPHA: Int = 30
internal const val DHT_CONCURRENCY: Int = 5
internal const val TIMEOUT: Int = 5 // in seconds
val LIBP2P_CERTIFICATE_EXTENSION: String = prefixToString()


class Lite internal constructor(
    private val keys: Keys = generateKeys(),
    private val bootstrap: List<Peeraddr> = emptyList(),
    private val peerStore: PeerStore = MemoryPeers(),
    internal val reserve: (Any) -> Unit = {}
) {

    private val certificate: Certificate = createCertificate(keys)
    private val connector: Connector = Connector(reserve)
    private val mutex = Mutex()

    /**
     * Find the peer addresses of given target peer ID via the relay.
     *
     * @param relay address of the relay which should be used to the relay connection
     * @param target the target peer ID which addresses should be retrieved

     * @return list of the peer addresses (usually one IPv6 address)
     */
    @Suppress("unused")
    suspend fun findPeer(relay: Peeraddr, target: PeerId): List<Peeraddr> {
        var connection: Connection? = null
        val signature = newSignature(keys, emptyList<Peeraddr>())
        val signatureMessage = relayMessage(signature, emptyList<Peeraddr>())
        try {
            connection = connect(this, relay)
            return connectHop(connection, target, signatureMessage)
        } finally {
            connection?.close()
        }
    }

    /**
     * Find the peer addresses of given target peer ID via the **libp2p** relay mechanism.
     *
     * @param target the target peer ID which addresses should be retrieved
     * @param timeout in seconds
     * @return list of the peer addresses (usually one IPv6 address)
     */
    suspend fun findPeer(target: PeerId, timeout: Long): List<Peeraddr> {
        val done = atomic<List<Peeraddr>>(emptyList<Peeraddr>())
        val signature = newSignature(keys, emptyList<Peeraddr>())
        val signatureMessage = relayMessage(signature, emptyList<Peeraddr>())
        val key = createPeerIdKey(target)

        val handled: MutableSet<PeerId> = ConcurrentHashMap.newKeySet()


        try {
            val scope = CoroutineScope(Dispatchers.IO)
            val channel: Channel<Connection> = Channel()
            withTimeout(timeout * 1000L) {

                scope.launch {
                    findClosestPeers(scope, channel, this@Lite, key)
                }

                for (connection in channel) {
                    if (!scope.isActive) {
                        break
                    }

                    try {
                        if (handled.add(connection.remotePeeraddr().peerId)) {
                            done.value = connectHop(connection, target, signatureMessage)
                            scope.cancel()
                        }
                    } catch (_: CancellationException) {
                        // ignore
                    } catch (throwable: Throwable) {
                        debug(throwable)
                    } finally {
                        connection.close()
                    }
                }
            }
        } catch (_: CancellationException) {
            // ignore
        } catch (throwable: Throwable) {
            debug(throwable)
        }
        return done.value
    }

    /**
     * Makes a reservation o relay nodes with the purpose that other peers can fin you via
     * the nodes peerId
     *
     * @param peeraddrs the peeraddrs which should be announced to incoming connecting peers via relays
     * @param maxReservation number of max reservations
     * @param timeout in seconds
     */
    suspend fun makeReservations(
        peeraddrs: List<Peeraddr>,
        maxReservation: Int,
        timeout: Int,
        running: (Boolean) -> Unit = {}
    ) {
        if (mutex.tryLock()) {
            try {
                running.invoke(true)
                makeReservations(this, peeraddrs, maxReservation, timeout)
            } catch (throwable: Throwable) {
                debug(throwable)
            } finally {
                running.invoke(false)
                mutex.unlock()
            }
        }
    }

    /**
     * Returns all currently connected relays as a list of peer addresses
     *
     * @return list of relay peer addresses
     */
    fun reservations(): List<Peeraddr> {
        return reservations(this)
    }

    fun hasReservations(): Boolean {
        return !reservations().isEmpty()
    }

    fun numReservations(): Int {
        return reservations().size
    }


    fun peerId(): PeerId {
        return keys.peerId
    }

    suspend fun shutdown() {
        connector.shutdown()
    }

    fun peerStore(): PeerStore {
        return peerStore
    }

    fun bootstrap(): List<Peeraddr> {
        return bootstrap
    }

    fun connector(): Connector {
        return connector
    }

    internal fun certificate(): Certificate {
        return certificate
    }

    fun keys(): Keys {
        return keys
    }
}

fun decodePeerId(name: String): PeerId {
    return decodePeerIdByName(name)
}


fun createPeeraddr(peerId: PeerId, address: ByteArray, port: UShort): Peeraddr {
    return Peeraddr(peerId, address, port)
}

fun createPeeraddr(peerId: String, address: ByteArray, port: UShort): Peeraddr {
    return createPeeraddr(decodePeerId(peerId), address, port)
}

/**
 * Create a new lite instance
 *
 * @param keys public and private ed25519 keys for the peer ID, signing, verification and authentication
 * @param bootstrap initial bootstrap peers for the DHT (without bootstrap peers it can only be used for testing)
 * @param peerStore additional DHT peers (note the list will be filled and readout)
 * @param reserve callback notification when number of reservations have changed
 */
fun newLite(
    keys: Keys = generateKeys(),
    bootstrap: List<Peeraddr> = emptyList(),
    peerStore: PeerStore = MemoryPeers(),
    reserve: (Any) -> Unit = {}
): Lite {
    return Lite(keys, bootstrap, peerStore, reserve)
}

class MemoryPeers : PeerStore {
    private val peers: MutableSet<Peeraddr> = ConcurrentHashMap.newKeySet()

    override suspend fun peeraddrs(limit: Int): List<Peeraddr> {
        return peers.take(limit).toList()
    }

    override suspend fun storePeeraddr(peeraddr: Peeraddr) {
        peers.add(peeraddr)
    }
}


private suspend fun makeReservations(
    lite: Lite,
    peeraddrs: List<Peeraddr>,
    maxReservation: Int,
    timeout: Int
) {

    val handledRelays: MutableSet<PeerId> = ConcurrentHashMap.newKeySet()

    // check if reservations are still valid and not expired
    for (connection in lite.connector().connections()) {
        if (connection.isMarked()) {
            handledRelays.add(connection.remotePeeraddr().peerId) // still valid
        }
    }
    val signature = newSignature(lite.keys(), peeraddrs)
    val signatureMessage = relayMessage(signature, peeraddrs)
    val valid = atomic(handledRelays.size)


    try {
        val scope = CoroutineScope(Dispatchers.IO)
        val key = createPeerIdKey(lite.peerId())
        // fill up reservations [not yet enough]
        withTimeout(timeout * 1000L) {

            val channel: Channel<Connection> = Channel()

            scope.launch {
                findClosestPeers(scope, channel, lite, key)
            }


            for (connection in channel) {
                // handled relays with given peerId
                if (!handledRelays.add(connection.remotePeeraddr().peerId)) {
                    break
                }

                if (valid.value > maxReservation) {
                    // no more reservations
                    break  // just return, let the refresh mechanism finished
                }

                if (!scope.isActive) {
                    break
                }

                // add stop handler to connection
                connection.responder().protocols.put(
                    RELAY_PROTOCOL_STOP, RelayStopHandler(
                        lite.peerId(), signatureMessage
                    )
                )

                scope.launch {
                    if (makeReservation(lite, connection)) {
                        if (valid.incrementAndGet() > maxReservation) {
                            // done
                            scope.cancel()
                        }
                    }
                }
            }
        }
    } catch (_: CancellationException) {
        // ignore
    } catch (throwable: Throwable) {
        debug(throwable)
    }
}


private suspend fun makeReservation(lite: Lite, connection: Connection): Boolean {
    connection.enableKeepAlive()
    try {
        reserveHop(connection, lite.peerId())
        connection.mark()
        lite.reserve.invoke(Any())
        return true
    } catch (_: Throwable) {
        connection.close()
        return false
    }
}

private fun reservations(lite: Lite): List<Peeraddr> {
    val peeraddrs = mutableListOf<Peeraddr>()
    for (connection in lite.connector().connections()) {
        if (connection.isMarked()) {
            peeraddrs.add(connection.remotePeeraddr())
        }
    }
    return peeraddrs
}

fun generateKeys(): Keys {
    val keyPair = Ed25519Sign.KeyPair.newKeyPair()
    return Keys(
        PeerId(keyPair.getPublicKey()),
        keyPair.getPrivateKey()
    )
}

fun verify(peerId: PeerId, data: ByteArray, signature: ByteArray) {
    val verifier = Ed25519Verify(peerId.hash)
    verifier.verify(signature, data)
}

fun sign(keys: Keys, data: ByteArray): ByteArray {
    val signer = Ed25519Sign(keys.privateKey)
    return signer.sign(data)
}

internal fun debug(message: String) {
    if (DEBUG) {
        println(message)
    }
}

internal fun debug(throwable: Throwable) {
    if (ERROR) {
        throwable.printStackTrace()
    }
}


private const val DEBUG: Boolean = true
private const val ERROR: Boolean = true