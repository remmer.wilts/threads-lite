package tech.lp2p.lite

import tech.lp2p.asen.PeerId
import tech.lp2p.asen.Peeraddr
import java.net.Inet6Address
import java.net.InetAddress
import java.net.NetworkInterface
import kotlin.random.Random


internal object TestEnv {
    const val ITERATIONS: Int = 4096
    val BOOTSTRAP: MutableList<Peeraddr> = mutableListOf()

    init {
        try {
            // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
            BOOTSTRAP.add(
                createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    InetAddress.getByName("104.131.131.82").address, 4001.toUShort()
                )
            )
        } catch (throwable: Throwable) {
            error(throwable)
        }
    }


    fun randomPeerId(): PeerId {
        return PeerId(getRandomBytes(32))
    }

    fun getRandomBytes(number: Int): ByteArray {
        val bytes = ByteArray(number)
        Random.nextBytes(bytes)
        return bytes
    }

    internal fun publicAddresses(): List<InetAddress> {
        val inetAddresses: MutableList<InetAddress> = ArrayList()

        try {
            val interfaces = NetworkInterface.getNetworkInterfaces()

            for (networkInterface in interfaces) {
                if (networkInterface.isUp) {
                    val addresses = networkInterface.inetAddresses
                    for (inetAddress in addresses) {
                        if (inetAddress is Inet6Address) {
                            if (!isLanAddress(inetAddress)) {
                                inetAddresses.add(inetAddress)
                            }
                        }
                    }
                }
            }
        } catch (throwable: Throwable) {
            throw IllegalStateException(throwable)
        }
        return inetAddresses
    }


    internal fun isLanAddress(inetAddress: InetAddress): Boolean {
        return inetAddress.isAnyLocalAddress
                || inetAddress.isLinkLocalAddress
                || inetAddress.isLoopbackAddress
                || inetAddress.isSiteLocalAddress
    }

    fun publicPeeraddrs(peerId: PeerId, port: Int): List<Peeraddr> {
        val inetSocketAddresses: Collection<InetAddress> = publicAddresses()
        val result = mutableListOf<Peeraddr>()
        for (inetAddress in inetSocketAddresses) {
            result.add(Peeraddr(peerId, inetAddress.address, port.toUShort()))
        }
        return result
    }

}
