The **Lite** library is currently in development [**Version 0.2.5**]

[[_TOC_]]

## Lite

The **Lite** library implements a subset of [libp2p](https://github.com/libp2p/specs/tree/master).

It is recommended to be familiar with the technology of **libp2p** in order to
understand this documentation.

### Goals

This sections describes the main goals of this project

- The library should be usable in the mobile context (Android). That requires that it is optimize
  towards memory usage and performance. [**Version 1.0.0**]
- The size of the library should be as small as possible. Reason is that it should be compiled as
  wasm library, and the more data are transferred in the Web context the less attractive the library
  will be (same goes for Android apps). The reduced size will be achieved by a well defined set of
  features and a pure kotlin implementation. [**Version 1.0.0**]
- The library should be compatible to **libp2p**, but with a defined subset of features [**Version
  1.0.0**]
- The library should be published to Maven Central [**Version 1.0.0**]
- The library should be in the final stage a Kotlin Multiplatform project [**Version 0.5.0**]

### Use-Cases

The only use-case which is supported, is the search of another peer via its peer ID.
To establish this behaviour a peer (B) which should be found, must be connected to relays
in the **libp2p** network (which are close in terms of its peer ID in a DHT).  
Another peer (B) wants to connect to peer (A), so it must know in advance its
peer ID. With this peer ID, peer (B) can connect to the same relays (like peer (A)) and establish
a connection via the relay.
Via the relay connection peer (A) receive from peer (B) its public addresses. The
relay connection is immediately closed after that.

**Limitation** The **libp2p** network protocol stack offers the possibility to establish a
direct connection between peer (A) and peer (B) via a **hole punch** mechanism. This mechanism seems
not very stable in the mobile context, so its not part of the library.

### ALPN "libp2p"

Application-Layer Protocol Negotiation (ALPN) is a Transport Layer Security (TLS) extension that
allows the application layer to negotiate which protocol should be performed over a secure
connection. This library support the ALPN **libp2p**.

The library support the following protocols of the ALPN **libp2p**

- /multistream/1.0.0
- /libp2p/circuit/relay/0.2.0/stop
- /libp2p/circuit/relay/0.2.0/hop
- /ipfs/kad/1.0.0
- /ipfs/id/1.0.0

### Certificate

The certificate, which is required for a TLS handshake during a connection process, is described
in [TLS Handshake](https://github.com/libp2p/specs/blob/master/tls/tls.md).
The self-signed certificate is used to verify each others peer Ids.

### Dependencies

The **Lite** library depends on the following libraries

- [Kwik](https://github.com/ptrd/kwik)
  [integrated, QUIC and TLS]

### Limitations

This sections contains the limitations of the library. The limitations are in general by design.

#### IP Version

Only IPv6 are supported, due to the fact that a direct connection of an IPv6 node to an IPv4 node is
not possible.

#### Protocols

The **ping**, **autonat**, **bitswap** and **pubsub**  and other protocols are not in the scope
of this library. [**Limitation by Design**]

#### Crypto

This library works only with **Ed25519** keys and only those will be supported. The keys are used
for signing own content (e.g. Certificate) and used for identification (PeerId, Peeraddr).

#### Transport

Only QUIC will be supported for transport, because of performance. [**Limitation by Design**]

#### MDNS

Not supported and it is not be considered to be part of the library. [**Limitation by Design**]

#### UPNP

Not supported and it is not be considered to be part of the library. [**Limitation by Design**]

## API

### General

The main functionality of the **Lite** library is available through the **tech.lp2p.lite.Lite**
class.

```

// generate the default required Ed25519 keys (PeerId and privateKey)
// Note: a PeerId is a Ed25519 public key 
val keys : Keys = generateKeys(); // generate new keys 

// bootstrap addresses for the DHT
val bootstrap = Peeraddrs();
bootstrap.add(...); // add a valid bootstrap address

val port = nextFreePort() // generate random port

val lite = newLite(keys = keys, bootstrap= bootstrap, blockStore = blockstore, port = port)


// -> or the shortform, which does the same settings
val lite = newLite(bootstrap= bootstrap); 



### All options


/**
 * Create a new lite instance
 *
 * @param keys public and private ed25519 keys for the peer ID, signing, verification and authentication
 * @param bootstrap initial bootstrap peers for the DHT (without bootstrap peers it can only be used for testing)
 * @param peerStore additional DHT peers (note the list will be filled and readout)
 * @param reserve callback notification when number of reservations have changed
 * @param port the port which is used for the "final" connection between two peers
 */
fun newLite(
    keys: Keys = generateKeys(),
    bootstrap: Peeraddrs = Peeraddrs(),   
    peerStore: PeerStore = MemoryPeers(),
    reserve: (Any) -> Unit = {},
    port: Int = nextFreePort()
): Lite {
...
}

```

#### Peer Store

This section describes the peer storage within this library.
Primarily it is used for storing peer within the **DHT** (**/ipfs/kad/1.0.0**) to fulfill its
functionality.

The default peer store implementation is represented by the class **MemoryPeers**.

```
interface PeerStore {
    fun peeraddrs(limit: Int): List<Peeraddr>

    fun storePeeraddr(peeraddr: Peeraddr)

    fun removePeeraddr(peeraddr: Peeraddr)
}
```

### Find Peer

This section describes how to find a peer via relays in a **libp2p2** network.

To find a peer the ID of the peer is required (peerId). A peerId is a a 32 bit Ed25519 public key,
which will also be used for signing content and authentication.

```
    /**
     * Find the peer addresses of given target peer ID via the **libp2p** relay mechanism.
     *
     * @param target the target peer ID which addresses should be retrieved
     * @param timeout in seconds
     * @return list of the peer addresses (usually one IPv6 address)
     */
    fun findPeer(target: PeerId, timeout: Long): Peeraddrs {
         ...
    }
```

### Reservation

This section describes how to monitor and initiate a reservation on relays.

A reservation to a relay is required, so that your node might be accessible by others nodes.

Documentation of relays are documented
under [circuit-v2](https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction).

```
     /**
     * Makes a reservation o relay nodes with the purpose that other peers can fin you via
     * the nodes peerId
     *
     * @param peeraddrs the peeraddrs which should be announced to incoming connecting peers via relays
     * @param maxReservation number of max reservations
     * @param timeout in seconds
     */
    fun makeReservations(
        peeraddrs: Peeraddrs,
        maxReservation: Int,
        timeout: Int,
        running: (Boolean) -> Unit = {}
    ) {
        ...
    }
    
    /**
     * Returns all currently connected relays as a list of peer addresses
     *
     * @return list of relay peer addresses
     */
    fun reservations(): Peeraddrs {
        ...
    }

    fun hasReservations(): Boolean {
        ...
    }
    
    fun numReservations(): Int {
        ...
    }
```

## Example

```
    @Test
    fun testConnection() {

        val bob = newLite(bootstrap = BOOTSTRAP)
        val alice = newLite(bootstrap = BOOTSTRAP)

        // Use Case : alice wants to connect to bob

        // [1] bob has to make reservations to relays
        
        // Note: bob has a service running on port 5001
        val publicAddresses = TestEnv.publicPeeraddrs(bob.peerId(), 5001) 
        bob.makeReservations(
            publicAddresses,
            20,
            120
        )  // timeout max 2 min (120 s) or 20 relays

        assertTrue(bob.hasReservations())

        // [2] alice can find bob via its peerId
        val peeraddrs = alice.findPeer(bob.peerId(), 120)  // timeout max 2 min (120 s)


        // testing
        assertNotNull(peeraddrs) // peeraddrs are the public IP addresses
        assertTrue(peeraddrs.isNotEmpty())

        val address = peeraddrs.first()
        assertEquals(address.peerId, bob.peerId())
        val inetAddress = InetAddress.getByAddress(address.address)
        assertNotNull(inetAddress)

        println("Found address $inetAddress")

        bob.close()
        alice.close()
    }

```
